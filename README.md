# Update
- Add [EVM stuff](https://gitlab.com/createlinx/omnia/omnia-token/-/blob/production/test/helper/evm.js) to mint in future or in past for tests
- update deployment scripts + [verify contract programmatically](https://hardhat.org/plugins/nomiclabs-hardhat-etherscan.html#using-programmatically)
- add .editorconfig to .prettierignore
- `npm i dotenv hardhat-contract-sizer hardhat-gas-reporter solidity-coverage chai-as-promised chai-string @nomiclabs/hardhat-ethers`
```
contractSizer: {
    alphaSort: true,
    runOnCompile: true,
    disambiguatePaths: false,
  },
  gasReporter: {
    currency: "USD",
    gasPrice: 21,
  },


networks: {
    hardhat: {
      live: false,
      saveDeployments: true,
      initialBaseFeePerGas: 0,
      tags: ["test", "local"],
    },
    ...
}   
```

# Configure GitLAb variables for docker-compose.yml tests

This repo is made to be a base template to create a smart contracts repo (only) with Gitlab CI to run lint, format & solidity-test jobs.

Linter: solhint<br>
Formatter: prettier<br>
Husky: do actions before commit, if fails doesn't commit<br>
Lint-staged: do actions only on modified files<br>

## Derivation path to get right address

Using Ledger path: `m/44'/60'/4'/0/0` where `4` is the index of the wanted address. Check [this](https://github.com/ethereum/EIPs/issues/84) for more info.

## What is a proxy?

A proxy is a contract that delegates all of its calls to a second contract, named an implementation contract. A proxy can be upgraded by its admin to use a different implementation contract.
**All state and funds are held in the proxy.**

## Migration to upgrade proxy

In fact all files are needed: the one that has deployed the base contract & the new one.
upgradeProxy() manage on its own which migration to use for update.

## Using proxy in the front or in tests

- Latest implementation ABI contract MUST be use in the front and test AT **proxy.address**

```
const v1 = await V1.at(proxy.address)
v1.oldMethod()
const v2 = await V2.at(proxy.address)
v2.newMethod()
```
