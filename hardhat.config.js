require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    version: "0.8.6",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },

  networks: {
    bsc_testnet: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      chainId: 97,
      accounts: {
        mnemonic: process.env.SEED,
        initialIndex: 0,
        // Using Ledger path
        // path: "m/44'/60'/0'/0"
      },
    },
    bsc: {
      url: "https://bsc-dataseed1.defibit.io/",
      chainId: 97,
      accounts: {
        mnemonic: process.env.SEED,
        initialIndex: 0,
        // Using Ledger path
        // path: "m/44'/60'/0'/0"
      },
    },
  },

  etherscan: {
    apiKey: process.env.BSC_SCAN_KEY,
  },
};
