const { expect } = require("chai");

let dai, swap;
const DAIaddress = "0x6b175474e89094c44da98b954eedeac495271d0f";

before(async () => {
  // Load Contracts
  const OmniaSwap = await ethers.getContractFactory("OmniaSwap");
  swap = await OmniaSwap.deploy();
  // dai = await ethers.
});

describe("DAI minting", async () => {
  it("should send ether to the Dai contract", async () => {
    // Send 1 eth to daiAddress to have gas to mint.
    // Uses ForceSend contract, otherwise just sending
    // a normal tx will revert.
    const forceSend = await ForceSend.new();
    await forceSend.go(dai.address, { value: Utils.toWei("1") });
    let ethBalance = await Utils.getETHBalance(dai.address);
    ethBalance = parseFloat(Utils.fromWei(ethBalance));
    expect(ethBalance).to.be.gte(1);
  });

  it("transfers 10 DAI to accounts[1]", async () => {
    await truffleAssert.passes(
      dai.transfer(accounts[1], Utils.toWei("10"), { from: DAIaddress })
    );

    let DAIBalance = await Utils.getDAIBalance(accounts[1]);
    DAIBalance = parseFloat(Utils.fromWei(DAIBalance));
    expect(DAIBalance).to.be.gte(1);
  });
});
